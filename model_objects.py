from enum import Enum
from typing import List


class Product:
    def __init__(self, name, unit):
        self.name = name
        self.unit = unit


class ProductQuantity:
    def __init__(self, product, quantity):
        self.product = product
        self.quantity = quantity


class ProductUnit(Enum):
    EACH = 1
    KILO = 2


class SpecialOfferType(Enum):
    THREE_FOR_TWO = 1
    TEN_PERCENT_DISCOUNT = 2
    TWO_FOR_AMOUNT = 3
    FIVE_FOR_AMOUNT = 4


class Offer:
    def __init__(self, offer_type: SpecialOfferType, product: Product, argument):
        self.offer_type = offer_type
        self.product = product
        self.argument = argument


class Discount:
    def __init__(self, product: Product, description: str, discount_amount: float):
        self.product = product
        self.description = description
        self.discount_amount = discount_amount


class Bundle:
    def __init__(self, products: List[Product], discount_percentage: float):
        self.products = products
        self.discount_percentage = discount_percentage
